<?php

declare(strict_types=1);

namespace PAB\Serializer;

interface RepresentationSerializerInterface
{
    public function representationSerialize($value);
}
