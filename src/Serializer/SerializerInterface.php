<?php

declare(strict_types=1);

namespace PAB\Serializer;

interface SerializerInterface
{
    public function serialize($value);
}
