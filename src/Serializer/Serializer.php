<?php

declare(strict_types=1);

namespace PAB\Serializer;

class Serializer extends AbstractSerializer implements SerializerInterface
{
    public function serialize($value)
    {
        return $this->serializeRecursively($value);
    }
}
