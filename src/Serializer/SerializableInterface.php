<?php

declare(strict_types=1);

namespace PAB\Serializer;

interface SerializableInterface
{
    public function toPab(): ?array;
}
