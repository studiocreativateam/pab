<?php

declare(strict_types=1);

namespace PAB\Serializer;

use PAB\Event;

interface PayloadSerializerInterface
{
    public function serialize(Event $event, bool $json = true): string|array;
}
