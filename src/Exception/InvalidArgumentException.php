<?php

declare(strict_types=1);

namespace PAB\Exception;

class InvalidArgumentException extends \InvalidArgumentException implements ExceptionInterface
{
}
