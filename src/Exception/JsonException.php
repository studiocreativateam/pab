<?php

declare(strict_types=1);

namespace PAB\Exception;

final class JsonException extends \Exception
{
}
