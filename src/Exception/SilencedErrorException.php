<?php

declare(strict_types=1);

namespace PAB\Exception;

final class SilencedErrorException extends \ErrorException
{
}
