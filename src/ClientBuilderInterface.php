<?php

declare(strict_types=1);

namespace PAB;

use Psr\Log\LoggerInterface;
use PAB\Serializer\RepresentationSerializerInterface;
use PAB\Serializer\SerializerInterface;
use PAB\Transport\TransportFactoryInterface;

interface ClientBuilderInterface
{
    public static function create(array $options = []): self;

    public function getOptions(): Options;

    public function getClient(): ClientInterface;

    public function setSerializer(SerializerInterface $serializer): self;

    public function setRepresentationSerializer(RepresentationSerializerInterface $representationSerializer): self;

    public function setLogger(LoggerInterface $logger): ClientBuilderInterface;

    public function setTransportFactory(TransportFactoryInterface $transportFactory): ClientBuilderInterface;

    public function setSdkIdentifier(string $sdkIdentifier): self;

    public function setSdkVersion(string $sdkVersion): self;
}
