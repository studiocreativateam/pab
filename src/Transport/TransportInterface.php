<?php

declare(strict_types=1);

namespace PAB\Transport;

use GuzzleHttp\Promise\PromiseInterface;
use PAB\Event;

interface TransportInterface
{
    public function send(Event $event): PromiseInterface;

    public function close(?int $timeout = null): PromiseInterface;
}
