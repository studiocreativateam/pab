<?php

declare(strict_types=1);

namespace PAB\Transport;

use GuzzleHttp\Promise\FulfilledPromise;
use GuzzleHttp\Promise\PromiseInterface;
use GuzzleHttp\Promise\RejectedPromise;
use Http\Client\HttpAsyncClient as HttpAsyncClientInterface;
use PAB\Event;
use PAB\EventType;
use PAB\Options;
use PAB\Response;
use PAB\ResponseStatus;
use PAB\Serializer\PayloadSerializerInterface;
use PAB\Util\JSON;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

final class HttpTransport implements TransportInterface
{
    /**
     * @var Options
     */
    private $options;

    /**
     * @var HttpAsyncClientInterface
     */
    private $httpClient;

    /**
     * @var StreamFactoryInterface
     */
    private $streamFactory;

    /**
     * @var RequestFactoryInterface
     */
    private $requestFactory;

    /**
     * @var PayloadSerializerInterface
     */
    private $payloadSerializer;

    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(
        Options                    $options,
        HttpAsyncClientInterface   $httpClient,
        StreamFactoryInterface     $streamFactory,
        RequestFactoryInterface    $requestFactory,
        PayloadSerializerInterface $payloadSerializer,
        ?LoggerInterface           $logger = null
    )
    {
        $this->options = $options;
        $this->httpClient = $httpClient;
        $this->streamFactory = $streamFactory;
        $this->requestFactory = $requestFactory;
        $this->payloadSerializer = $payloadSerializer;
        $this->logger = $logger ?? new NullLogger();
    }

    public function send(Event $event): PromiseInterface
    {
        $authData = $this->options->getAuthData();
        $content = $this->payloadSerializer->serialize($event);
        $request = null;
        if (!empty($authData->getToken()) && !empty($authData->getPrivateKey())) {
            if (EventType::transaction() === $event->getType()) {
                $request = $this->requestFactory->createRequest('POST', $authData->getTransactionApiEndpointUrl())
                    ->withHeader('Content-Type', 'application/json')
                    ->withHeader('Authorization', 'Bearer '.$authData->getPrivateKey())
                    ->withBody($this->streamFactory->createStream($content));
            } else {
                $request = $this->requestFactory->createRequest('POST', $authData->getProjectApiEndpointUrl())
                    ->withHeader('Content-Type', 'application/json')
                    ->withHeader('Authorization', 'Bearer '.$authData->getPrivateKey())
                    ->withBody($this->streamFactory->createStream($content));
            }
        }
        try {
            /** @var ResponseInterface $response */
            $response = $this->httpClient->sendAsyncRequest($request)->wait();
        } catch (\Throwable $exception) {
            $this->logger->error(
                sprintf('Failed to send the event to Pab. Reason: "%s".', $exception->getMessage()),
                ['exception' => $exception, 'event' => $event]
            );

            return new RejectedPromise(new Response(ResponseStatus::failed(), $event));
        }

        $sendResponse = new Response(ResponseStatus::createFromHttpStatusCode($response->getStatusCode()), $event);

        if (ResponseStatus::success() === $sendResponse->getStatus()) {
            return new FulfilledPromise($sendResponse);
        }

        return new RejectedPromise($sendResponse);
    }

    public function close(?int $timeout = null): PromiseInterface
    {
        return new FulfilledPromise(true);
    }
}
