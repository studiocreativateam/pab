<?php

declare(strict_types=1);

namespace PAB\Transport;

use PAB\Options;

interface TransportFactoryInterface
{
    public function create(Options $options): TransportInterface;
}
