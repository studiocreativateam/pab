<?php

declare(strict_types=1);

namespace PAB\Transport;

use GuzzleHttp\Promise\FulfilledPromise;
use GuzzleHttp\Promise\PromiseInterface;
use PAB\Event;
use PAB\Response;
use PAB\ResponseStatus;

final class NullTransport implements TransportInterface
{
    public function send(Event $event): PromiseInterface
    {
        return new FulfilledPromise(new Response(ResponseStatus::skipped(), $event));
    }

    public function close(?int $timeout = null): PromiseInterface
    {
        return new FulfilledPromise(true);
    }
}
