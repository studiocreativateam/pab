<?php

declare(strict_types=1);

namespace PAB\HttpClient\Authentication;

use Http\Message\Authentication as AuthenticationInterface;
use Psr\Http\Message\RequestInterface;
use PAB\Options;

final class PABAuthentication implements AuthenticationInterface
{
    /**
     * @var Options
     */
    private $options;

    /**
     * @var string
     */
    private $sdkIdentifier;

    /**
     * @var string
     */
    private $sdkVersion;

    public function __construct(Options $options, string $sdkIdentifier, string $sdkVersion)
    {
        $this->options = $options;
        $this->sdkIdentifier = $sdkIdentifier;
        $this->sdkVersion = $sdkVersion;
    }

    public function authenticate(RequestInterface $request): RequestInterface
    {
        $authData = $this->options->getAuthData();

        if (null === $authData) {
            return $request;
        }

        return $request->withHeader('Authorization', 'Bearer ' . $authData->getPrivateKey());
    }
}
