<?php

declare(strict_types=1);

namespace PAB\HttpClient;

use Http\Client\HttpAsyncClient as HttpAsyncClientInterface;
use PAB\Options;

interface HttpClientFactoryInterface
{
    public function create(Options $options): HttpAsyncClientInterface;
}
