<?php

declare(strict_types=1);

namespace PAB\Integration;

interface IntegrationInterface
{
    public function setupOnce(): void;
}
