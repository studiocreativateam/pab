<?php

declare(strict_types=1);

namespace PAB\Integration;

use PAB\Event;
use PAB\ExceptionMechanism;
use PAB\State\HubInterface;
use PAB\State\Scope;

abstract class AbstractErrorListenerIntegration implements IntegrationInterface
{
    protected function captureException(HubInterface $hub, \Throwable $exception): void
    {
        $hub->withScope(function (Scope $scope) use ($hub, $exception): void {
            $scope->addEventProcessor(\Closure::fromCallable([$this, 'addExceptionMechanismToEvent']));

            $hub->captureException($exception);
        });
    }

    protected function addExceptionMechanismToEvent(Event $event): Event
    {
        $exceptions = $event->getExceptions();

        foreach ($exceptions as $exception) {
            $exception->setMechanism(new ExceptionMechanism(ExceptionMechanism::TYPE_GENERIC, false));
        }

        return $event;
    }
}
