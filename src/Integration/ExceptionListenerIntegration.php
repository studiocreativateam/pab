<?php

declare(strict_types=1);

namespace PAB\Integration;

use PAB\ErrorHandler;
use PAB\PABSdk;

final class ExceptionListenerIntegration extends AbstractErrorListenerIntegration
{
    public function setupOnce(): void
    {
        $errorHandler = ErrorHandler::registerOnceExceptionHandler();
        $errorHandler->addExceptionHandlerListener(static function (\Throwable $exception): void {
            $currentHub = PABSdk::getCurrentHub();
            $integration = $currentHub->getIntegration(self::class);

            if (null === $integration) {
                return;
            }

            $integration->captureException($currentHub, $exception);
        });
    }
}
