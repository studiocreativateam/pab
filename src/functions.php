<?php

declare(strict_types=1);

namespace PAB;

use PAB\Tracing\Transaction;
use PAB\Tracing\TransactionContext;

function init(array $options = []): void
{
    $client = ClientBuilder::create($options)->getClient();
    PABSdk::init()->bindClient($client);
}

function captureMessage(string $message, ?Severity $level = null, ?EventHint $hint = null): ?EventId
{
    return PABSdk::getCurrentHub()->captureMessage($message, $level, $hint);
}

function captureException(\Throwable $exception, ?EventHint $hint = null): ?EventId
{
    return PABSdk::getCurrentHub()->captureException($exception, $hint);
}

function captureEvent(Event $event, ?EventHint $hint = null): ?EventId
{
    return PABSdk::getCurrentHub()->captureEvent($event, $hint);
}

function captureLastError(?EventHint $hint = null): ?EventId
{
    return PABSdk::getCurrentHub()->captureLastError($hint);
}

function addBreadcrumb(Breadcrumb $breadcrumb): void
{
    PABSdk::getCurrentHub()->addBreadcrumb($breadcrumb);
}

function configureScope(callable $callback): void
{
    PABSdk::getCurrentHub()->configureScope($callback);
}

function withScope(callable $callback): void
{
    PABSdk::getCurrentHub()->withScope($callback);
}

function startTransaction(TransactionContext $context, array $customSamplingContext = []): Transaction
{
    return PABSdk::getCurrentHub()->startTransaction($context, $customSamplingContext);
}
