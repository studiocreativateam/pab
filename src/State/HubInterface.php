<?php

declare(strict_types=1);

namespace PAB\State;

use PAB\Breadcrumb;
use PAB\ClientInterface;
use PAB\Event;
use PAB\EventHint;
use PAB\EventId;
use PAB\Integration\IntegrationInterface;
use PAB\Severity;
use PAB\Tracing\SamplingContext;
use PAB\Tracing\Span;
use PAB\Tracing\Transaction;
use PAB\Tracing\TransactionContext;

interface HubInterface
{
    public function getClient(): ?ClientInterface;
    
    public function getLastEventId(): ?EventId;

    public function pushScope(): Scope;

    public function popScope(): bool;

    public function withScope(callable $callback): void;
    
    public function configureScope(callable $callback): void;

    public function bindClient(ClientInterface $client): void;

    public function captureMessage(string $message, ?Severity $level = null/*, ?EventHint $hint = null*/): ?EventId;

    public function captureException(\Throwable $exception/*, ?EventHint $hint = null*/): ?EventId;

    public function captureEvent(Event $event, ?EventHint $hint = null): ?EventId;

    public function captureLastError(/*?EventHint $hint = null*/): ?EventId;

    public function addBreadcrumb(Breadcrumb $breadcrumb): bool;

    public function getIntegration(string $className): ?IntegrationInterface;

    public function startTransaction(TransactionContext $context/*, array $customSamplingContext = []*/): Transaction;

    public function getTransaction(): ?Transaction;

    public function getSpan(): ?Span;

    public function setSpan(?Span $span): HubInterface;
}
