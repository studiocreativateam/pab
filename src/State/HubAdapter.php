<?php

declare(strict_types=1);

namespace PAB\State;

use PAB\Breadcrumb;
use PAB\ClientInterface;
use PAB\Event;
use PAB\EventHint;
use PAB\EventId;
use PAB\Integration\IntegrationInterface;
use PAB\PABSdk;
use PAB\Severity;
use PAB\Tracing\Span;
use PAB\Tracing\Transaction;
use PAB\Tracing\TransactionContext;

final class HubAdapter implements HubInterface
{
    /**
     * @var self|null
     */
    private static $instance;

    private function __construct()
    {
    }

    public static function getInstance(): self
    {
        if (null === self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function getClient(): ?ClientInterface
    {
        return PABSdk::getCurrentHub()->getClient();
    }

    public function getLastEventId(): ?EventId
    {
        return PABSdk::getCurrentHub()->getLastEventId();
    }

    public function pushScope(): Scope
    {
        return PABSdk::getCurrentHub()->pushScope();
    }

    public function popScope(): bool
    {
        return PABSdk::getCurrentHub()->popScope();
    }

    public function withScope(callable $callback): void
    {
        PABSdk::getCurrentHub()->withScope($callback);
    }

    public function configureScope(callable $callback): void
    {
        PABSdk::getCurrentHub()->configureScope($callback);
    }

    public function bindClient(ClientInterface $client): void
    {
        PABSdk::getCurrentHub()->bindClient($client);
    }

    public function captureMessage(string $message, ?Severity $level = null, ?EventHint $hint = null): ?EventId
    {
        return PABSdk::getCurrentHub()->captureMessage($message, $level, $hint);
    }

    public function captureException(\Throwable $exception, ?EventHint $hint = null): ?EventId
    {
        return PABSdk::getCurrentHub()->captureException($exception, $hint);
    }

    public function captureEvent(Event $event, ?EventHint $hint = null): ?EventId
    {
        return PABSdk::getCurrentHub()->captureEvent($event, $hint);
    }

    public function captureLastError(?EventHint $hint = null): ?EventId
    {
        return PABSdk::getCurrentHub()->captureLastError($hint);
    }

    public function addBreadcrumb(Breadcrumb $breadcrumb): bool
    {
        return PABSdk::getCurrentHub()->addBreadcrumb($breadcrumb);
    }

    public function getIntegration(string $className): ?IntegrationInterface
    {
        return PABSdk::getCurrentHub()->getIntegration($className);
    }

    public function startTransaction(TransactionContext $context, array $customSamplingContext = []): Transaction
    {
        return PABSdk::getCurrentHub()->startTransaction($context, $customSamplingContext);
    }

    public function getTransaction(): ?Transaction
    {
        return PABSdk::getCurrentHub()->getTransaction();
    }

    public function getSpan(): ?Span
    {
        return PABSdk::getCurrentHub()->getSpan();
    }

    public function setSpan(?Span $span): HubInterface
    {
        return PABSdk::getCurrentHub()->setSpan($span);
    }

    public function __clone()
    {
        throw new \BadMethodCallException('Cloning is forbidden.');
    }

    public function __wakeup()
    {
        throw new \BadMethodCallException('Unserializing instances of this class is forbidden.');
    }
}
